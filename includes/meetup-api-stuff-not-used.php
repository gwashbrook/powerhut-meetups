<?php
// add_action( 'init', getMeetups() );
function getMeetups( $args = array() ) {

	/**
	 * Define the array of defaults
	 */ 
	$defaults = array (
	
		'key' => PHUT_MEETUP_API_KEY,
		'group_urlname' => 'WorthingDigital-Meetup',
		'status' => 'past,upcoming',

	
	);
	
	/**
 	* Parse incoming $args into an array and merge it with $defaults
	*/ 
	$args = wp_parse_args( $args, $defaults );
	


	$meetup = new Meetup(array(
		'key' => $args['key'],
	));


	$meetup_args = array(
	
		'group_urlname' => $args['group_urlname'],
		'status' => $args['status']
	
	
	);


	$response = $meetup->getEvents( $meetup_args );

	// total number of items matching the get request
	$total_count = $response->meta->total_count;
	
	
	if( $total_count > 0 ) {
		processResponse( $response );
		while( ( $response = $meetup->getNext( $meetup_args ) ) !== null ) {
			processResponse( $response );
		}
	}

} // fn getMeetups




function processResponse( $response ) {
  			foreach($response->results as $event) {
				processMeetup( $event );
			}
}

function processMeetup( $event ) {

	// echo $event->name . ' : ' . date('l jS F, Y g:i a', $event->time / 1000) . '<br />';


	$post = array (
		'post_author' => 1,
		'post_content' => '',
		// 'post_content_filtered'
		'post_title' => wp_strip_all_tags( $event->name ),
		'post_excerpt' => '',
		'post_status' => 'draft',
		'post_type' => 'phut_meetup',
		'comment_status' => 'closed',
		'ping_status' => 'closed',
	);
	$post_id = wp_insert_post( $post );


}

//consumer key 27p8s95iqh28uoc6bvo5gd1m18
// secret tfv7nf71vu5cmi5shgifhq0ipl


// add_action ('save_post_phut_meetup','tell_meetup');
function tell_meetup(){


	$args = array (
		'description' => '',
		'duration' => '7200000',
		'group_id' => '',
		'group_urlname' => 'Worthing-WordPress',
		'guest_limit' => '',
		'hosts' => '',
		'how_to_find_us' => '',
		'name' => '',
		'publish_status' => 'draft',
		'simple_html_description' => '',
		'time' => '',
		'venue_id' => '', 
		'venue_visibility' => 'members',
		'why' => '',
	
	);





} // 