<?php

function phut_meetups_register_cpt() {
	
	$supports = array (
		'title',
		'excerpt',
		'editor',
		'author',
		'thumbnail',
		'custom_fields',
		// 'comments',
		// 'revisions',
		// 'genesis_seo',                      //
		// 'genesis-layouts',                  //
		// 'genesis-simple-sidebars',          //
		'genesis-scripts',                  //
		'genesis-cpt-archives-settings',    //
		// 'genesis-entry-meta-after-content', //
		// 'autodescription-meta',             // The SEO Framework
		// 'page-attributes',                  // menu order ( hierarchical post types only )
		// 'genesis-adjacent-entry-nav',
	);
	
	$labels = array (
		'name'               => 'Meetups', // Shown in breadcrumbs, archive title, admin list table, menu items
		'singular_name'      => 'Meetup',
		'menu_name'          => 'Meetups',
		'name_admin_bar'     => 'Meetup',
		'all_items'          => 'All Meetups',
		'add_new'            => 'Add New',
	 	'add_new_item'       => 'Add New Meetup',
		'edit_item'          => 'Edit Meetup',
		'new_item'           => 'New Meetup',
		'view_item'          => 'View Meetup',
		'search_items'       => 'Search Meetups',
		'not_found'          => 'No Meetups Found',
		'not_found_in_trash' => 'No Meetups found in Trash',
		// 'parent_item_colon'  => 'Parent Meetup', // only used in hierarchical post types	
	);
	
	$rewrite = array (
		'slug'              => 'meetups',
		'with_front'        => false, // eg: if permalink structure is /blog/, then links will be: false->/news/, true->/blog/news/). Defaults to true
		// 'with_front' => true,
		'feeds'             => false, // Defaults to has_archive value
		'pages'             => true, // Should the permalink structure provide for pagination. Defaults to true	
	);
	
	$taxonomies = array (
		'phut_meetup_category',
	);
	
	$args = array (
		'label'             => 'Meetup',
		'labels'            => $labels,
		'description'       => '', // (string) (optional) A short descriptive summary of what the post type is. May be visible, e.g. in menus
		'public'            => true, // true || false - optional : Needs to be true for Yoast
			'exclude_from_search' => false, // true?
			'publicly_queryable'  => true,
			'show_in_nav_menus'   => true, // Preferably true if has_archive is true
			'show_ui'             => true,
		'show_in_menu'      => true,
		'show_in_admin_bar' => true,
		// 'menu_position'        => NULL, // Defauts to below Comments	
		'menu_icon' => 'dashicons-groups',
		'capability_type' => 'phut_meetup', // referenced
		// 'capability_type' => 'post',
		
		'map_meta_cap'         => true,
		'hierarchical' => false,
		'supports' => $supports,
		// 'register_meta_box_cb' => '', // Called when setting up the meta boxes for the edit form. The callback function takes one argument $post, which contains the WP_Post object for the currently edited post. Do remove_meta_box() and add_meta_box() calls in the callback. 
		'taxonomies' => $taxonomies,
		'has_archive' => true,
		'rewrite' => $rewrite,	
		'query_var' => true, // defaults to true (custom post type name)
		'can_export' => true, // default true	
		'show_in_rest' => false, // default false
		// 'rest_base' => 'restbase',
		
		/* TO MAKE IT PUBLIC WITH ARCHIVE
		'publicly_queryable' => true,
		'has_archive' => true,
		*/
	);
	
	register_post_type( 'phut_meetup', $args );
	
	// Make sure comment out or remove flush_rewrite when not in development
	flush_rewrite_rules();
	
} //fn