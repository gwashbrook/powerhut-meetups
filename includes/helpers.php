<?php

/*
 *
 */
function phut_meetups_meetup_summary() {

	printf( '<article %s>', genesis_attr( 'entry' ) ); ?>

	<div class="meetup-body">
		<?php genesis_do_post_title() ?>
		<?php
		do_action( 'genesis_before_entry_content' );
		printf( '<div %s>', genesis_attr( 'entry-content' ) );
		do_action( 'genesis_entry_content' );
		?>
		<p><a class="button" href="<?php the_permalink() ?>" title="<?php the_title_attribute() ?>">View meetup</a></p>
        <?php
		echo '</div>';
		do_action( 'genesis_after_entry_content' );
		?>
	</div>

	<?php if( has_post_thumbnail()) : ?>
		<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" class="meetup-image-permalink"><?php the_post_thumbnail('meetup-archive', array('itemprop'=>'image')); ?></a>
	<?php endif; ?>



<?php
	echo '</article>';
} //fn



function phut_meetup_ldjson_schema() {
?>
<script type="application/ld+json">
// <![CDATA[
{
"@context": "http://schema.org",
"@type": "Event",
"name": "<?php esc_attr_e( get_the_title() ) ?>",
"startDate": "<?php esc_attr_e( get_field( 'date_time_start' ) ) ?>",
"endDate": "<?php esc_attr_e( get_field( 'date_time_end' ) ) ?>",
"description": "This is the description of the event, We don't have to keep it short - yet using the excerpt may be a good thing",
"url": "<?php esc_attr_e( get_permalink() ) ?>",
"sameAs": "<?php esc_attr_e( get_field( 'offer_url' ) ) ?>",
"location": {
	"@type": "Place",
	"name": "<?php esc_attr_e( get_field( 'location_name' ) ) ?>",
	"address": {
		"@type": "PostalAddress",
		"streetAddress": "<?php esc_attr_e( get_field( 'street_address' ) ) ?>",
		"addressLocality": "<?php esc_attr_e( get_field( 'address_locality' ) ) ?>",
		"addressRegion": "<?php esc_attr_e( get_field( 'address_region' ) ) ?>",
		"postalCode": "<?php esc_attr_e( get_field( 'postal_code' ) ) ?>",
		"addressCountry": "<?php esc_attr_e( get_field( 'address_country' ) ) ?>"
	}
},
"organizer": {
	"@type": "Organization",
	"name": "Powerhut Net",
	"url": "https://powerhut.net/",
	"address": {
		"@type": "PostalAddress",
		"streetAddress": "The Mill Building, 31 - 35 Chatsworth Road",
		"addressLocality": "Worthing",
		"addressRegion": "West Sussex",
		"postalCode": "BN11 1LY",
		"addressCountry": "GB"
	}
},
"performer": {
	"@type": "Organization",
	"name": "Worthing WordPress",
	"url": "https://worthingwp.com/"
},
"inLanguage": "en-GB",
"offers": {
	"@type": "Offer",
	"description": "Free",
	"price": "0",
	"priceCurrency": "GBP",
	"url": "<?php esc_attr_e( get_field( 'offer_url' ) ) ?>"
}
}
// ]]>
</script>
<?php
}