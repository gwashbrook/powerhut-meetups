<?php

function phut_meetups_register_taxonomies(){
	
	$labels = array (
		'name' => 'Meetup Categories',
		'singular_name' => 'Meetup Category',
		'menu_name' => 'Categories',
		'all_items' => 'All Categories',
		'edit_item' => 'Edit Category',		
		'view_item' => 'View Category',
		'update_item' => 'Update Category',
		'add_new_item' => 'Add New Meetup Category',
		'new_item_name' => 'New Category',
		'parent_item' => 'Parent Category',
		'parent_item_colon' => 'Parent Category:',			
		'search_items' => 'Search Meetup Categories',
		'popular_items' => 'Popular Categories',
		//'separate_items_with_commas' => 'Separate tags with commas',
		//'add_or_remove_items' => 'Add or remove categories',
		//'choose_from_most_used'  => 'Choose from the most used categories',	
	);

	// (boolean or array) (optional) Set to false to prevent automatic URL rewriting a.k.a. "pretty permalinks".
	// Pass an $args array to override default URL settings for permalinks. Default: true 
	$rewrite = array (
		'slug' => 'meetup-category', // - Used as pretty permalink text (i.e. /tag/) - defaults to $taxonomy (taxonomy's name slug)
		'with_front' => true, // - allowing permalinks to be prepended with front base - defaults to true ??
		'hierarchical' => true, // - true or false allow hierarchical urls (implemented in Version 3.1)

	);
	
	$capabilities = array (
		'manage_terms' => 'manage_meetup_categories',
		'edit_terms' => 'edit_meetup_categories',
		'delete_terms' => 'delete_meetup_categories',
		'assign_terms' => 'assign_meetup_categories',	
	);

	$args = array (
		'labels'            => $labels,
		'public'            => true, // optional
			'show_in_nav_menus' => true, // defaults to value of public
			'show_ui'           => true, // defaults to value of public
				'show_tagcloud'     => false, // defaults to value of show_ui - Whether to allow the Tag Cloud widget to use this taxonomy
				'show_in_quick_edit' => true, // defaults to value of show_ui
		// 'meta_box_cb'       => '', // Defaults to post_tags_meta_box() or post_categories_meta_box(). No metabox is shown if set to false
		'show_admin_column' => true, // automatic creation of taxonomy columns on associated posts table
		'hierarchical'      => true,
		
		// 'update_count_callback' => '', // Works like a hook
		'query_var'         => false,
		'capabilities'      => $capabilities,
		'rewrite'           => $rewrite,
		// 'sort' => false, // default none // Whether this taxonomy should remember the order in which terms are added to objects
		
		
		'show_in_rest'       => false,
    	// 'rest_base'          => 'meetup-category',
			
	);


	register_taxonomy( 'phut_meetup_category', array( 'phut_meetup' ), $args );




	
}