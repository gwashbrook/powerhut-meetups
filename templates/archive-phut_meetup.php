<?php

// Set Empty schema as using JSON-LD
add_filter( 'genesis_attr_entry', 'phut_core_schema_empty', 20 );
add_filter( 'genesis_attr_entry-title', 'phut_core_schema_empty', 20 );
add_filter( 'genesis_attr_entry-content', 'phut_core_schema_empty', 20 );

// Remove potentially conflicting generic schema - its all about the context
add_filter( 'genesis_attr_body', 'phut_core_schema_empty', 20 );
add_filter( 'genesis_attr_site-header', 'phut_core_schema_empty', 20 );
add_filter( 'genesis_attr_nav-primary', 'phut_core_schema_empty', 20 );
add_filter( 'genesis_attr_nav-secondary', 'phut_core_schema_empty', 20 );
add_filter( 'genesis_attr_content', 'phut_core_schema_empty', 20 );
add_filter( 'genesis_attr_sidebar-primary', 'phut_core_schema_empty', 20 ); // Primary sidebar?!!
add_filter( 'genesis_attr_site-footer', 'phut_core_schema_empty',20 );


//* Remove breadcrumbs
remove_action( 'genesis_before_loop', 'genesis_do_breadcrumbs' );

//* Remove the entry meta from the entry header (requires HTML5 theme support)
remove_action( 'genesis_entry_header', 'genesis_post_info', 12 );

//* Add meetup meta to ht entry header
add_action( 'genesis_entry_header', 'phut_meetup_info_summary', 12 );
function phut_meetup_info_summary(){
?>
<p class="entry-meta">
<strong><?php echo date('D jS M, Y ~ g:i a', strtotime( get_field( 'date_time_start' ) ) ) ?></strong><br />
<strong><?php the_field('location_name'); ?></strong>, <?php the_field('address_locality'); ?> <?php the_field('postal_code'); ?>
</p>
<?php	
}


//* Add schema json
add_action( 'genesis_entry_footer', phut_meetup_ldjson_schema );

// add_action('genesis_entry_footer','myFn' );
function myFn() {
	echo sprintf( '<a href="%s" rel="bookmark" class="button button-sm">%s</a>', get_permalink(), 'Read more' );
	echo sprintf( '<a href="%s" target="_blank" class="button button-sm button-danger">%s</a>', get_field('offer_url'), 'Meetup' );
}

// 2017-01-12 21:00:00





//* Sort meetups by custom
add_action( 'genesis_before_loop', 'ntg_do_query' );
function ntg_do_query() {
	global $query_string;
	query_posts( wp_parse_args( $query_string, array(
		'meta_type' => 'DATETIME',
		'meta_key'	=> 'date_time_start',
		'orderby'	=> 'meta_value',
		'order'		=> 'DESC'
		
	) ) );
}

genesis();