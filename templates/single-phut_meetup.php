<?php
//* Force full-width-content layout
// add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );




//* Set Event schema

// Set Empty schema as using JSON-LD
add_filter( 'genesis_attr_entry', 'phut_core_schema_empty', 20 );
add_filter( 'genesis_attr_entry-title', 'phut_core_schema_empty', 20 );
add_filter( 'genesis_attr_entry-content', 'phut_core_schema_empty', 20 );


// Remove potentially conflicting generic schema - its all about the context
add_filter( 'genesis_attr_body', 'phut_core_schema_empty', 20 );
add_filter( 'genesis_attr_site-header', 'phut_core_schema_empty', 20 );
add_filter( 'genesis_attr_nav-primary', 'phut_core_schema_empty', 20 );
add_filter( 'genesis_attr_nav-secondary', 'phut_core_schema_empty', 20 );
add_filter( 'genesis_attr_content', 'phut_core_schema_empty', 20 );
add_filter( 'genesis_attr_sidebar-primary', 'phut_core_schema_empty', 20 ); // Primary sidebar?!!
add_filter( 'genesis_attr_site-footer', 'phut_core_schema_empty', 20 );


// Customise genesis_attr
add_filter( 'genesis_attr_entry','myFn');
function myFn( $attributes ){
	$attributes['class'] = 'entry type-phut_meetup';
	return $attributes;	
}

//* Remove the post info function from the entry header
remove_action ('genesis_meta','child_maybe_move_post_info' );
remove_action( 'genesis_entry_header', 'genesis_post_info', 12 );


add_action('wp_head','phut_meetup_wp_head');
function phut_meetup_wp_head () {
?>
<style type="text/css">
.acf-map {
	width: 100%;
	height: 400px;
	border: #ccc solid 1px;
	margin-bottom: 1.5em;
}
.acf-map img {
   max-width: inherit !important;
}
</style>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDHFm_SNoDrJt4pGFCkSxxH1iD67fybxc4"></script>
<script type="text/javascript">
(function($) {
function new_map( $el ) {
	var $markers = $el.find('.marker');
	var args = {
		zoom		: 16,
		center		: new google.maps.LatLng(0, 0),
		mapTypeId	: google.maps.MapTypeId.ROADMAP
	};
	var map = new google.maps.Map( $el[0], args);
	map.markers = [];
	$markers.each(function(){
    	add_marker( $(this), map );
	});
	center_map( map );
	return map;
}
function add_marker( $marker, map ) {
	var latlng = new google.maps.LatLng( $marker.attr('data-lat'), $marker.attr('data-lng') );
	var marker = new google.maps.Marker({
		position	: latlng,
		map			: map
	});
	map.markers.push( marker );
	if( $marker.html() )
	{
		var infowindow = new google.maps.InfoWindow({
			content		: $marker.html()
		});
		google.maps.event.addListener(marker, 'click', function() {
			infowindow.open( map, marker );
		});
	}
}
function center_map( map ) {
	var bounds = new google.maps.LatLngBounds();
	$.each( map.markers, function( i, marker ){
		var latlng = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );
		bounds.extend( latlng );
	});
	if( map.markers.length == 1 )
	{
	    map.setCenter( bounds.getCenter() );
	    map.setZoom( 16 );
	}
	else
	{
		map.fitBounds( bounds );
	}
}
var map = null;
$(document).ready(function(){
	$('.acf-map').each(function(){
		map = new_map( $(this) );
	});
});
})(jQuery);
</script>


<?php
}


add_action( 'genesis_entry_content', 'phut_meetup_info', 5 );
function phut_meetup_info () {
	phut_meetup_ldjson_schema();
	$location = get_field('location');

?>
<div>
<table class="table-bordered table-striped">
<tbody>
<tr>
<th>When:</th>
<td><?php echo date('l jS F, Y', strtotime( get_field( 'date_time_start' ) ) ) ?><br /><?php echo date('g:i a', strtotime( get_field( 'date_time_start' ) ) ) ?> to <?php echo date('g:i a', strtotime( get_field( 'date_time_end' ) ) ) ?></td>
</tr>
<tr>
<th>Where:</th>
<td>
<?php the_field('location_name'); ?><br />
<?php the_field('street_address'); ?><br />
<?php the_field('address_locality'); ?><br />
<?php the_field('address_region'); ?><br />
<?php the_field('postal_code'); ?>
</td>
</tr>
</tbody>
</table>
<ul class="inline">
<li><a href="<?php esc_attr_e( get_field('offer_url') ) ?>" target="_blank" rel="nofollow" class="button meetup">View event on meetup.com</a></li>
<?php if( !empty($location) ): ?>
<li><a href="#meetupmap" class="button">Map</a></li>
<?php endif; ?>
</ul>
</div>
<?php
}



add_action( 'genesis_after_entry_content','phut_meetup_map', 9);
function phut_meetup_map () {
	$location = get_field('location');
	if( !empty($location) ):
?>
<a class="anchor" name="meetupmap"></a>
<div class="acf-map"><div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"><h4><?php the_field('location_name') ?></h4><p class="address"><?php echo $location['address']; ?></p></div></div>
<?php endif; ?>

<p><a href="<?php esc_attr_e( get_field('offer_url') ) ?>" target="_blank" rel="nofollow" class="button meetup">View event on meetup.com</a></p>

<?php
}



genesis();
