<?php
/*
Plugin Name: Powerhut Meetups
Description: Adds the phut_meetup custom post type and associated taxonomies
Plugin URI: https://powerhut.net/plugins
Author: Graham Washbrook
Author URI: https://powerhut.net
Text Domain: powerhut-meetups
Domain Path: /languages
Version: 0.0.2
*/

// namespace = ns_ = phut_meetups_
// NAMESPACE = NS_ = PHUT_MEETUPS
// cpt_name = phut_meetup
// projects = meetups
// project = meetup
// taxonomy_category_name =


//* Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;


//* Define plugin path
define ('PHUT_MEETUPS_PATH', plugin_dir_path( __FILE__ ) );


//* Includes and requires
require( PHUT_MEETUPS_PATH . 'includes/config.php');
require( PHUT_MEETUPS_PATH . 'includes/helpers.php');
require( PHUT_MEETUPS_PATH . 'includes/type-taxonomies.php');
require( PHUT_MEETUPS_PATH . 'includes/type-cpt.php');
require( PHUT_MEETUPS_PATH . 'includes/widgets/widgets.php');
require( PHUT_MEETUPS_PATH . 'includes/meetup.php');


//* On plugin activation
function phut_meetups_activation() {

    if ( ! current_user_can( 'activate_plugins' ) ) return;
	
	phut_meetups_add_caps();

	phut_meetups_init();
	flush_rewrite_rules();
}
register_activation_hook( __FILE__, 'phut_meetups_activation' );


//* On plugin deactivation
function phut_meetups_deactivation() {

	phut_meetups_remove_caps();
	flush_rewrite_rules();


}
register_deactivation_hook( __FILE__, 'phut_meetups_deactivation' );


//* On plugin uninstall
function phut_meetups_uninstall() {
	
	/*
	// e.g. delete options
	$option_name = 'wporg_option';
 	delete_option($option_name);
 
	// for site options in Multisite
	delete_site_option($option_name);
 
	// eg. drop a custom database table
	global $wpdb;
	$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}mytable");
	*/	
}
register_uninstall_hook( __FILE__, 'phut_meetups_uninstall');


//* Add capabilites
function phut_meetups_add_caps() {

	$role_names = array (
		'administrator',
		'editor'
	);

	foreach( $role_names as $role_name ) {
	
		$role = get_role( $role_name );
	
		$role->add_cap( 'edit_phut_meetup' );
		$role->add_cap( 'read_phut_meetup' );
		$role->add_cap( 'delete_phut_meetup' );
		$role->add_cap( 'edit_phut_meetups' );
		$role->add_cap( 'edit_others_phut_meetups' );
		$role->add_cap( 'publish_phut_meetups' );
		$role->add_cap( 'read_private_phut_meetups' );
		$role->add_cap( 'delete_phut_meetups' );
		$role->add_cap( 'delete_private_phut_meetups' );
		$role->add_cap( 'delete_published_phut_meetups' );
		$role->add_cap( 'delete_others_phut_meetups' );
		$role->add_cap( 'edit_private_phut_meetups' );
		$role->add_cap( 'edit_published_phut_meetups' );

		$role->add_cap( 'manage_meetup_categories' );
		$role->add_cap( 'edit_meetup_categories' );
		$role->add_cap( 'delete_meetup_categories' );
		$role->add_cap( 'assign_meetup_categories' );
	}
}

//* Remove capabilities
function phut_meetups_remove_caps() {

	$role_names = array (
		'administrator',
		'editor'
	);

	foreach( $role_names as $role_name ) {

    	$role = get_role( $role_name );

		$role->remove_cap( 'edit_phut_meetup' );
		$role->remove_cap( 'read_phut_meetup' );
		$role->remove_cap( 'delete_phut_meetup' );
		$role->remove_cap( 'edit_phut_meetups' );
		$role->remove_cap( 'edit_others_phut_meetups' );
		$role->remove_cap( 'publish_phut_meetups' );
		$role->remove_cap( 'read_private_phut_meetups' );
		$role->remove_cap( 'delete_phut_meetups' );
		$role->remove_cap( 'delete_private_phut_meetups' );
		$role->remove_cap( 'delete_published_phut_meetups' );
		$role->remove_cap( 'delete_others_phut_meetups' );
		$role->remove_cap( 'edit_private_phut_meetups' );
		$role->remove_cap( 'edit_published_phut_meetups' );


		$role->remove_cap( 'manage_meetup_categories' );
		$role->remove_cap( 'edit_meetup_categories' );
		$role->remove_cap( 'delete_meetup_categories' );
		$role->remove_cap( 'assign_meetup_categories' );
		
	}
}



//* Use custom templates
add_filter('single_template', 'phut_meetups_custom_templates');
add_filter('taxonomy_template', 'phut_meetups_custom_templates');
add_filter('archive_template', 'phut_meetups_custom_templates');

function phut_meetups_custom_templates( $template ) {

	if ( is_singular('phut_meetup') )
		return PHUT_MEETUPS_PATH . 'templates/single-phut_meetup.php';

	if ( is_post_type_archive('phut_meetup') || is_tax('phut_meetup_category') )
		return PHUT_MEETUPS_PATH . 'templates/archive-phut_meetup.php';

	return $template;

}



//* Init
function phut_meetups_init() {
	
	// Register taxonomies
	phut_meetups_register_taxonomies();
	
	// Register post types
	phut_meetups_register_cpt();
	
	// Better be safe than sorry when registering custom taxonomies for custom post types.
	register_taxonomy_for_object_type( 'phut_meetup_category', 'phut_meetup' );
	
	// Change enter title here text
	add_filter( 'enter_title_here', 'phut_meetups_enter_title_here' );
	
	// Change the post updated messages
	add_filter( 'post_updated_messages', 'phut_meetups_updated_messages' );
	
	// Change the bulk post updated messages
	add_filter( 'bulk_post_updated_messages', 'phut_meetups_bulk_updated_messages' );
	
	// Add image size
	// add_image_size( 'meetup-archive', 640, 480, array( 'left', 'top' ) ); // Hard crop left top
	
	
} //fn
add_action( 'init', 'phut_meetups_init', 0 );


function phut_meetups_enter_title_here( $title ){
	$screen = get_current_screen();
	
	if( $screen->post_type == 'phut_meetup' )
		$title = 'Enter Meetup title here';
		
	return $title;	
}


function phut_meetups_updated_messages( $messages ){
	global $post;
	
	// Add messages to $messages array
	$messages['phut_meetup'] = array(
		0  => '', // Unused. Messages start at index 1.
		1  => sprintf( 'Meetup updated. <a href="%s">View meetup</a>', esc_url( get_permalink( $post->ID ) ) ),
		2  => 'Custom field updated.',
		3  => 'Custom field deleted.',
		4  => 'Meetup updated.',
		5  => isset( $_GET['revision']) ? sprintf( 'Meetup restored to revision from %s', wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6  => sprintf( 'Meetup published. <a href="%s">View meetup</a>', esc_url( get_permalink( $post->ID ) ) ),
		7  => 'Meetup saved.',
		8  => sprintf( 'Meetup submitted. <a target="_blank" href="%s">Preview meetup</a>', esc_url( add_query_arg( 'preview', 'true', get_permalink( $post->ID ) ) ) ),
		9  => sprintf( 'Meetup publishing scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview meetup</a>', date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink( $post->ID ) ) ),
		10 => sprintf( 'Meetup draft updated. <a target="_blank" href="%s">Preview meetup</a>', esc_url( add_query_arg( 'preview', 'true', get_permalink( $post->ID ) ) ) ),
	);	

	return $messages;	
}


function phut_meetups_bulk_updated_messages( $bulk_messages ){
	global $bulk_counts;
	
	// Add messages to $bulk_messages array
	$bulk_messages['phut_meetup'] = array(
		'updated'   => _n( '%s meetup updated.', '%s meetups updated.', $bulk_counts['updated'] ),
		'locked'    => _n( '%s meetup not updated, somebody is editing it.', '%s meetups not updated, somebody is editing them.', $bulk_counts['locked'] ),
		'deleted'   => _n( '%s meetup permanently deleted.', '%s meetups permanently deleted.', $bulk_counts['deleted'] ),
		'trashed'   => _n( '%s meetup deleted permanently.', '%s meetups deleted permanently.', $bulk_counts['trashed'] ),	
		// 'untrashed' => _n( '%s meetup restored from Trash.', '%s meetups restored from the Trash.', $bulk_counts['untrashed'] ),
	);
	
	return $bulk_messages;
}



